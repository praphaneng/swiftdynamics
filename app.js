const express = require("express");
const request = require("request");
var $ = require("jquery");
const { json } = require("body-parser");
const app = express();
const port = 3000;

app.get("/answer", function (req, res) {
  let tempResult;
  let id = {
    min: "",
    max: "",
    avg: "",
  };
  let data = {
    min: "",
    max: "",
    avg: "",
  };
  let data2 = {
    min: "",
    max: "",
    avg: "",
  };
  let chunk = [];
  let predicData;

  let resultResult = { id, data, data2, predicData, chunk };

  request(
    "http://3.1.189.234:8091/data/ttntest",
    function (error, response, body) {
      console.error("error:", error);
      tempResult = JSON.parse(body);

      console.log("tempResult", tempResult);

      id.min = findMinID(tempResult);
      id.max = findMaxID(tempResult);
      id.avg = findAverageID(tempResult);

      data.min = findMinData(tempResult);
      data.max = findMaxData(tempResult);
      data.avg = findAverageData(tempResult);

      data2.min = findMinData2(tempResult);
      data2.max = findMaxData2(tempResult);
      data2.avg = findAverageData2(tempResult);

      chunk.push(chunkData(tempResult));

      resultResult.predicData = predictionData(
        tempResult[Object.keys(tempResult)[Object.keys(tempResult).length - 1]]
      );

      if (resultResult) res.json(resultResult);
      else {
        res.json("null");
      }
    }
  );
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

function findMinID(arrObj) {
  return (min = Math.min.apply(
    null,
    arrObj.map(function (item) {
      return item.id;
    })
  ));
}

function findMinData(arrObj) {
  return (min = Math.min.apply(
    null,
    arrObj.map(function (item) {
      return item.data;
    })
  ));
}

function findMinData2(arrObj) {
  return (min = Math.min.apply(
    null,
    arrObj.map(function (item) {
      if (item.data2 != null) return item.data2;
    })
  ));
}

function findMaxID(arrObj) {
  return Math.max.apply(
    null,
    arrObj.map(function (item) {
      return item.id;
    })
  );
}

function findMaxData(arrObj) {
  return Math.max.apply(
    null,
    arrObj.map(function (item) {
      return item.data;
    })
  );
}

function findMaxData2(arrObj) {
  return Math.max.apply(
    null,
    arrObj.map(function (item) {
      return item.data2;
    })
  );
}

function findAverageID(arrObj) {
  let sum = 0;
  let avg = arrObj.reduce((sum, { id }) => sum + id, 0) / arrObj.length;
  return avg;
}

function findAverageData(arrObj) {
  let sum = 0;
  let avg = arrObj.reduce((sum, { data }) => sum + data, 0) / arrObj.length;
  return avg;
}

function findAverageData2(arrObj) {
  let sum = 0;
  let avg = arrObj.reduce((sum, { data2 }) => sum + data2, 0) / arrObj.length;
  return avg;
}

function chunkData(arrObj) {
  let packChunk = [];
  let i,
    j,
    tempArr,
    chunk = 200;
  for (i = 0, j = arrObj.length; i < j; i += chunk) {
    tempArr = arrObj.slice(i, i + chunk);
    packChunk.push(tempArr);
  }
  return packChunk;
}

function predictionData(LastItem) {
  let predictionData = {
    predictData1Day: "",
    predictData7Day: "",
  };
  const generatorFunc = counter(LastItem.data);

  function* counter(value) {
    let step;

    while (true) {
      step = yield ++value;

      if (step) {
        value += step;
      }
    }
  }
  predictionData.predictData1Day = generatorFunc.next().value;
  predictionData.predictData7Day = generatorFunc.next(7).value;

  return predictionData;

  console.log(predictionData);
}
